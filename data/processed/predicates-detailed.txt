has role	4396
has participant	3592
has assay method	2037
has assay format	1095
has assay control	1068
has measured entity	928
has manufacturer	917
has function	895
has assay kit	741
has mode of action	675
has concentration unit	642
has detection method	622
has assay readout content parametricity	617
has assay phase characteristic	615
is bioassay type of	607
has organism	593
uses detection instrument	590
has concentration throughput	582
has quality	581
has repetition throughput	575
has assay readout content	520
assay measurement type	507
has assay footprint	486
has bioassay type	406
has cell line	405
has preparation method	397
DNA construct	352
has percent response	317
has response unit	307
has confirmatory assay	246
compound library	222
has assay stage	218
has primary assay	189
involves biological process	171
has purity value	122
has alternate target assay	115
has assay serum	113
has inducer	60
has alternate assay conditions	24
has compound toxicity assay	16
has orthogonal assay technology	15
has alternate cell line assay	14
has orthogonal assay design	12
has variant construct assay	10
has lead optimization assay	10
has physicochemical profiling assay	9
has target	8
has alternate assay format	7
primary cell	4
has temperature unit	2
has alternate confirmatory assay	1
has unit	1
has time unit	1
disease	1
has selectivity assay	1
