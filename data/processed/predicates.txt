has role
has participant
has assay method
has assay format
has assay control
has measured entity
has manufacturer
has function
has assay kit
has mode of action
has concentration unit
has detection method
has assay readout content parametricity
has assay phase characteristic
is bioassay type of
has organism
uses detection instrument
has concentration throughput
has quality
has repetition throughput
has assay readout content
assay measurement type
has assay footprint
has bioassay type
has cell line
has preparation method
DNA construct
has percent response
has response unit
has confirmatory assay
compound library
has assay stage
has primary assay
involves biological process
has purity value
has alternate target assay
has assay serum
has inducer
has alternate assay conditions
has compound toxicity assay
has orthogonal assay technology
has alternate cell line assay
has orthogonal assay design
has variant construct assay
has lead optimization assay
has physicochemical profiling assay
has target
has alternate assay format
primary cell
has temperature unit
has alternate confirmatory assay
has unit
has time unit
disease
has selectivity assay
