from src.data import prepare_mapping, check_entities
from src.models import to_onnx


def main():
    check_entities.main()
    prepare_mapping.main()
    to_onnx.main()


if __name__ == '__main__':
    main()
