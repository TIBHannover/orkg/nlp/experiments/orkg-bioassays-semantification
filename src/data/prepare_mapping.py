import os

from src import RAW_DATA_DIR, PROCESSED_DATA_DIR
from src.util.io import read_json, read_txt, write_json

cluster_num = 700

PREDICATES_FILE = os.path.join(PROCESSED_DATA_DIR, 'orkg-predicates.json')
RESOURCES_FILE = os.path.join(PROCESSED_DATA_DIR, 'orkg-resources.json')
LABELS_DIR = os.path.join(RAW_DATA_DIR, 'labels')


def convert_to_labels(lines, orkg_predicates, orkg_resources):
    labels_object = {}
    labels_list = []

    for line in lines:
        line = line.strip()
        toks = line.split("\t")
        text = toks[0]
        frequency = int(toks[1])

        if frequency >= 1:
            if "||" in text:
                # line with nested statements
                # they are not returned as objects because the nested statements are precreated
                # only the top-level node is returned
                labels = text.split("||")
                label_parts = labels[0].split(" ; # ")[1].split(' -> ')
            else:
                label_parts = text.split(" ; # ")[1].split(' -> ')

            lhs = label_parts[0].strip().strip('\"')
            rhs = label_parts[1].strip().strip('\"')

            if lhs not in labels_object:
                labels_object[lhs] = {
                    'id': orkg_predicates[lhs],
                    'resources': [{
                        'id': orkg_resources[rhs],
                        'label': rhs
                    }]
                }
            else:
                labels_object[lhs]['resources'].append({
                        'id': orkg_resources[rhs],
                        'label': rhs
                    })

    for key in labels_object:
        labels_list.append({
            'property': {
                'id': labels_object[key]['id'],
                'label': key,
            },
            'resources': labels_object[key]['resources']
        })

    return {
        "labels": labels_list,
    }


def main():
    json_data = {}
    orkg_predicates = read_json(PREDICATES_FILE)
    orkg_resources = read_json(RESOURCES_FILE)

    for k in range(0, cluster_num):
        lines = read_txt(os.path.join(LABELS_DIR, '{}.labels'.format(k)))
        json_data[k] = convert_to_labels(lines, orkg_predicates, orkg_resources)

    write_json(json_data, os.path.join(PROCESSED_DATA_DIR, 'mapping.json'))


if __name__ == '__main__':
    main()
