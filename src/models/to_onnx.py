import pickle
import os

from onnxconverter_common import StringTensorType
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
from skl2onnx.helpers.onnx_helper import add_output_initializer

from src import MODELS_DIR

MODEL_PATH = os.path.join(MODELS_DIR, 'orkgnlp-bioassays-kmeans-700.pkl')
VECTORIZER_PATH = os.path.join(MODELS_DIR, 'orkgnlp-bioassays-tfidf.pkl')


def read_pickle(input_path):
    with open(input_path, 'rb') as f:
        loaded_object = pickle.load(f)
    return loaded_object


def write_onnx(model, input_path):
    with open(input_path, 'wb') as f:
        f.write(model.SerializeToString())


def convert_model(model, model_path, input_tensor_type, additional_output=None, metadata=None):
    print("converting {}".format(model_path))

    onnx_model = convert_sklearn(model,
                                 initial_types=[('X', input_tensor_type)]
                                 )
    if additional_output:
        onnx_model = add_output_initializer(onnx_model, additional_output[0], additional_output[1])

    if metadata:
        for key, value in metadata.items():
            meta = onnx_model.metadata_props.add()
            meta.key = key
            meta.value = value

    onnx_model_path = '{}.onnx'.format(os.path.splitext(model_path)[0])
    write_onnx(onnx_model, onnx_model_path)

    print('converted {}'.format(onnx_model_path))


def main():
    # ----------- Sklearn-KMeans -----------
    model = read_pickle(MODEL_PATH)
    convert_model(
        model=model,
        model_path=MODEL_PATH,
        input_tensor_type=FloatTensorType([None, model.cluster_centers_.shape[1]]),
        metadata={
            'description': 'sklearn.cluster.KMeans ONNX formatted model. (see sklearn docs '
                           'for more details).',
            'author': 'Omar Arab Oghli <omar.araboghli@tib.eu>',
            'organization': 'Open Research Knowledge Graph https://www.orkg.org',
            'license': 'MIT',
            'model_version': 'v0.1.0',
            'model_release': 'https://gitlab.com/TIBHannover/orkg/nlp/experiments/orkg-bioassays-semantification'
                             '/-/releases/v0.1.0'
        }
    )

    # ----------- Sklearn-TfidfVectorizer -----------
    model = read_pickle(VECTORIZER_PATH)
    convert_model(
        model=model,
        model_path=VECTORIZER_PATH,
        input_tensor_type=StringTensorType([None, 1]),
        metadata={
            'description': 'sklearn.feature_extraction.text.TfidfVectorizer ONNX formatted model '
                           '(see sklearn docs for more details).',
            'author': 'Omar Arab Oghli <omar.araboghli@tib.eu>',
            'organization': 'Open Research Knowledge Graph https://www.orkg.org',
            'license': 'MIT',
            'model_version': 'v0.1.0',
            'model_release': 'https://gitlab.com/TIBHannover/orkg/nlp/experiments/orkg-bioassays'
                             '-semantification/-/releases/v0.1.0'
        }
    )


if __name__ == '__main__':
    main()
