import json


def read_txt(path):
    with open(path, 'r') as f:
        lines = f.readlines()
    return lines


def read_json(input_path):
    with open(input_path, encoding='utf-8') as f:
        json_data = json.load(f)

    return json_data


def write_json(json_data, output_path):
    with open(output_path, 'w') as json_file:
        json.dump(json_data, json_file, indent=4)
