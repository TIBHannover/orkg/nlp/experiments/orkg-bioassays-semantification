# ORKG BioAssays Semantification



####  $`\textcolor{red}{\text{WARNING}}`$

> This ``README`` is not sufficient to re-create the dataset or re-train
the models because of missing information while migrating the codebase from the
old repository ([``orkg-nlp-experiments``](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-experiments)).
The [``main.py``](src/main.py) script is intended to prepare the service for 
integration into the [``orkgnlp``](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-pypi) package.



## Overview

### Aims
The bioassay semantification service automatically semantifies bioassay descriptions based on the semantic model
of the [Bioassay ontology](http://bioassayontology.org/). More information on the supporting clustering algorithm
of the service implementation, its development gold-standard dataset,
and its performance results can be found in our [publication](https://arxiv.org/abs/2111.15182).

### Approach
``...``

### Dataset
``...``

### Limitations 
``...``

### Useful Links
* ``...``
* ``...``

## How to Run

### Prerequisites

* Python version ``^3.7.1``.


Running the main script with the following commands will create the needed
[``mapping.json``](./data/processed/mapping.json) file for the prediction
and convert the pickled models to an ONNX format.

```commandline
git clone https://gitlab.com/TIBHannover/orkg/nlp/experiments/orkg-bioassays-semantification/
cd orkg-bioassays-semantification
pip install -r requirements.txt
python -m src.main
```



## Contribution
This service is developed and maintained by:
* D'Souza, Jennifer <jennifer.dsouza@tib.eu>
* Arab Oghli, Omar <omar.araboghli@tib.eu>

## License
[MIT](./LICENSE)

## How to Cite

```commandline
@article{DBLP:journals/corr/abs-2111-15182,
  author       = {Marco Anteghini and
                  Jennifer D'Souza and
                  V{\'{\i}}tor A. P. Martins dos Santos and
                  S{\"{o}}ren Auer},
  title        = {Easy Semantification of Bioassays},
  journal      = {CoRR},
  volume       = {abs/2111.15182},
  year         = {2021},
  url          = {https://arxiv.org/abs/2111.15182},
  eprinttype    = {arXiv},
  eprint       = {2111.15182},
  timestamp    = {Fri, 18 Mar 2022 10:22:08 +0100},
  biburl       = {https://dblp.org/rec/journals/corr/abs-2111-15182.bib},
  bibsource    = {dblp computer science bibliography, https://dblp.org}
}
```

## References

* [Easy Semantification of Bioassays](https://arxiv.org/abs/2111.15182)

